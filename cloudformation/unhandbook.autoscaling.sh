#!/bin/sh

SG_NAME="UN-Handbook-WebServerGroup-1F0KYB4UUSA6"

# Set up scaling policies
SCALE_UP_POLICY=`as-put-scaling-policy UNHScaleUpPolicy1 \
    --auto-scaling-group $SG_NAME --adjustment=2 --type ChangeInCapacity \
    --cooldown 180 --region us-west-1 --aws-credential-file /opt/aws/credential-file-path.template`

mon-put-metric-alarm UNH-HighCPUAlarm1 --comparison-operator GreaterThanThreshold \
    --evaluation-periods 1 --metric-name CPUUtilization --namespace "AWS/EC2" \
    --period 180 --statistic Average --threshold 75 \
    --alarm-actions $SCALE_UP_POLICY \
    --dimensions "AutoScalingGroupName=$SG_NAME" \
    --region us-west-1 \
    --aws-credential-file /opt/aws/credential-file-path.template

SCALE_DOWN_POLICY=`as-put-scaling-policy UNHScaleDownPolicy1 \
    --auto-scaling-group $SG_NAME --adjustment=-2 --type ChangeInCapacity \
    --cooldown 300 --region us-west-1 --aws-credential-file /opt/aws/credential-file-path.template`

mon-put-metric-alarm UNH-LowCPUAlarm1 --comparison-operator LessThanThreshold \
    --evaluation-periods 1 --metric-name CPUUtilization --namespace "AWS/EC2" \
    --period 300 --statistic Average --threshold 25 \
    --alarm-actions $SCALE_DOWN_POLICY \
    --dimensions "AutoScalingGroupName=$SG_NAME" \
    --region us-west-1 \
    --aws-credential-file /opt/aws/credential-file-path.template